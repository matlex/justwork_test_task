from __future__ import unicode_literals

from django.apps import AppConfig


class YandexSearchScraperConfig(AppConfig):
    name = 'yandex_search_scraper'
