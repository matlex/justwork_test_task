from __future__ import unicode_literals

from django.db import models


class YandexSearchResults(models.Model):
    query_string = models.CharField(max_length=255)
    page_number = models.IntegerField()
    result_url = models.CharField(max_length=255)
    result_title = models.CharField(max_length=100)
    added = models.DateTimeField(auto_now_add=True)
