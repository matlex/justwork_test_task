# *-* coding: utf-8 *-*
from grab import Grab
from lxml.html import fromstring
import random
import time
from urlparse import urljoin
from yandex_search_scraper.models import YandexSearchResults
from django.core.management.base import BaseCommand

ya_ru_start_page = "https://ya.ru/"
yandex_primary_domain = "https://yandex.ru/"
br = Grab()


def get_html(url):
    """
    Requests given url and returns response html
    :param url:
    """
    try:
        delay = random.randrange(1, 10, _int=float)
        print "Delay is:", delay
        time.sleep(delay)
        print "Acessing URL:", url
        response = br.go(url)
        return response.body
    except Exception as e:
        print e
        return False


def make_search_request(search_string):
    """
    :param search_string
    Makes search request with the given search_string and returns html body with results
    """
    get_html(ya_ru_start_page)
    br.doc.set_input_by_id("text", search_string)
    br.doc.submit()
    time.sleep(5)
    return br.doc.body


def get_next_page_url(html):
    """
    Takes input html and extracts a next page url from bottom pagination line
    :param html:
    """
    parsed_body = fromstring(html)
    pagination_pages = parsed_body.xpath('//a[@class="link link_ajax_yes pager__item pager__item_kind_next i-bem"]/@href')
    return pagination_pages[0]


def search_page_results_handler(html):
    """
    :param html:
    Takes a html page with results. Scrapes them and outputs list of dicts.
    Dict format is {"result_title": value, "result_url": value}
    """
    output = []
    parsed_body = fromstring(html)
    results = parsed_body.xpath('//h2[@class="serp-item__title"]')
    for h2 in results:
        result_title = h2.xpath('a')[0].text_content()
        result_url = h2.xpath('a/@href')[0]
        output.append({
            "result_title": result_title,
            "result_url": result_url
        })
    return output


def save_results_to_db(results, page_number, query_string):
    """
    Saves provided results into YandexSearchResults table via Django ORM
    :param query_string:
    :param page_number:
    :param results:
    """
    for result in results:
        obj = YandexSearchResults(
            query_string=query_string,
            page_number=page_number,
            result_url=result.get("result_url"),
            result_title=result.get("result_title")
        )
        obj.save()
    print "Results from page#{} saved successfully!".format(page_number)


def main():
    # Some pre-defined variables
    page_num = 0
    pages_to_scrape = 3
    query_string = u"фантастика"

    # First of all we need make a search call
    response_html = make_search_request(query_string)

    while True:
        # Manipulate with received data
        output_results = search_page_results_handler(response_html)

        # We need to add 1 to page_num for visual consistency because pagination starts from #1 page
        save_results_to_db(output_results, int(page_num)+1, query_string)

        # Then obtain next page url
        next_page_url = get_next_page_url(response_html)
        next_page_url = urljoin(yandex_primary_domain, next_page_url)
        page_num += 1

        # Check if we achieved target pages for scraping
        if page_num >= pages_to_scrape:
            break

        # Then request a next page url
        response_html = get_html(next_page_url)


class Command(BaseCommand):
    def handle(self, *args, **options):
        main()

